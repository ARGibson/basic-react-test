var React = require('react');
var ReactDOM = require('react-dom');
var createStore = require('redux').createStore;
var CakeListContainer = require('./containers/CakeListContainer').default;
var Reducer = require('./reducers/Index').default;

let store = createStore(Reducer);

ReactDOM.render(<CakeListContainer store={store} />, document.getElementById('cakeList'));