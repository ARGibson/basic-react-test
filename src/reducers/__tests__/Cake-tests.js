jest.unmock('../Cake');
jest.unmock('../../actions/CakeActions');

import {addCake} from '../../actions/CakeActions';
import CakeReducer from '../Cake';

describe('cakeReducer', () => {
  it('should return the inital state', () => {
    expect(CakeReducer(undefined, {}))
    .toEqual([]);
  });

  it('should handle ADD_CAKE', () => {
    const cake = {
      title: 'Really good noms',
      image: 'noms.jpg'
    };

    expect(CakeReducer([], addCake(cake)))
      .toEqual([
        {
          title: cake.title,
          image: cake.image
        }
      ]);
  });
});